extends Button

export (String) var up_next: String

func _ready() -> void:
	connect('pressed', self, 'switch_scene')

func switch_scene() -> void:
	# get_tree().change_scene_to(up_next)
	get_tree().paused = false
	Global.switch_scene(up_next)
