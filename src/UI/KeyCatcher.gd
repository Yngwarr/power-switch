extends ConfirmationDialog

signal action_remapped

var action: String
var current_event: InputEvent
var msg_format: String
var caught_event: InputEvent

func _ready() -> void:
	connect('about_to_show', self, 'reset')
	connect('confirmed', self, 'write_action')
	msg_format = dialog_text + ' %s'
	reset()

func set_action(name: String, e: InputEvent) -> void:
	action = name
	current_event = e

func reset() -> void:
	dialog_text = msg_format % '...'
	caught_event = null

func write_action() -> void:
	InputMap.action_erase_event(action, current_event)
	InputMap.action_add_event(action, caught_event)
	emit_signal('action_remapped', action, caught_event, current_event)

func catchable_event(e: InputEvent) -> bool:
	return e is InputEventKey || e is InputEventJoypadButton || e is InputEventJoypadMotion

func _input(event: InputEvent) -> void:
	if !visible: return
	if event.is_action_pressed('pause'): return
	if !catchable_event(event): return
	get_tree().set_input_as_handled()
	caught_event = event
	dialog_text = msg_format % Global.event_as_text(event)
