extends Node2D

export (NodePath) var sfx_switch: NodePath
export (NodePath) var music_switch: NodePath

onready var sfx_switch_node: CheckButton = get_node(sfx_switch)
onready var music_switch_node: CheckButton = get_node(music_switch)
onready var NewGame: Button = $UILayer/Menu/MainMenu/NewGame

func _ready() -> void:
	NewGame.connect('visibility_changed', self, 'focus_button')

	if sfx_switch_node == null:
		printerr("can't load a node: '%s'" % sfx_switch)
		return
	if music_switch_node == null:
		printerr("can't load a node: '%s'" % music_switch)
		return

	sfx_switch_node.pressed = Global.options['sfx']
	music_switch_node.pressed = Global.options['music']

func focus_button():
	NewGame.grab_focus()
