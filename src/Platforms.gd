extends StaticBody2D

export (NodePath) var occluders_path

onready var platforms = get_children()
onready var occluders = get_node(occluders_path)

func _ready():
	for p in platforms:
		var occl = LightOccluder2D.new()
		occl.occluder = OccluderPolygon2D.new()
		occl.occluder.polygon = p.polygon
		occl.occluder.cull_mode = 2
		occluders.add_child(occl)
