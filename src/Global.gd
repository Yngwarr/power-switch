extends Node

# game's options
var options := {
	sfx = true,
	music = false
}

var game_over := false
var current_scene = null

func _ready() -> void:
	# TODO load options from disk
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func set_option(key: String, value) -> void:
	# TODO save options to disk
	options[key] = value

# transition between the scenes
func switch_scene(path: String) -> void:
	call_deferred('_deferred_switch_scene', path)

func _deferred_switch_scene(path: String) -> void:
	current_scene.free()

	var s = ResourceLoader.load(path)
	current_scene = s.instance()

	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)

# -------- UTILS --------

# TODO move where it belongs (idk)
func event_as_text(event: InputEvent) -> String:
	if event is InputEventJoypadButton:
		return "Joypad %s" % event.button_index
	if event is InputEventJoypadMotion:
		return "Axis %s%s" % [event.axis, '-' if event.axis_value < 0 else '+']
	return event.as_text()
